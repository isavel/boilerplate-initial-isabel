import React, { Component } from 'react';

class AsyncRequest extends Component {
  state = {
    data: null,
    error: null,
    show: false,
  }

  componentDidMount() {
    console.log('componentDidMount: ', this.state);
  }

  asyncRequest = async () => {
    // const cloe = 'https://cloemx.wcaas.net/api/wcaas/mongoApi/cloemx/page/productsByCategory/mujer/calzado?lang=es_mx&currency=mxn&page=1&limit=30';
    const chapur = 'https://chapurmx.wcaas.net/api/wcaas/mongoApi/chapurmx/page/productsByCategory/moda-mujer/ropa/blusas-y-playeras?lang=es_mx&currency=mxn&page=1&limit=30';
    try {
      const url = chapur;
      const req = await fetch(url);
      const res = await req.json();
      this.setState({ data: res, show: true });
    } catch (err) {
      this.setState({ error: err, show: false });
    } finally {
      console.log('asyncRequest is done ', this.state);
    }
  }

  render() {
    console.log('render');
    const { data, show } = this.state;
    const element = (data !== null) ? <p style={{ color: 'green' }}>Loaded! see console for logs</p> : <p style={{ color: 'grey' }}>loading...</p>;
    return (
      <main>
        <h1>Async/Await</h1>
        <a onClick={this.asyncRequest} style={{ display: 'inline-block', padding: '1rem', backgroundColor: 'steelblue', color: 'white', margin: '1rem 0' }}>getApi</a>
        {show ? element : ''}
      </main>
    );
  }
}

export default AsyncRequest;
