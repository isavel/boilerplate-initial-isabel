import React, { Component } from 'react';
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import 'typeface-lato';
import 'typeface-roboto-slab';
import './sass/App.scss';

// Layout
import Layout from './components/Layout/Layout';
import LayoutDefault from './components/Layout/layout-default';
import LayoutCheckout from './components/Layout/layout-checkout';
import LayoutCustom from './components/Layout/layout-custom';
import LayoutBlank from './components/Layout/layout-blank';

// Landing
import Home from './components/Landing/Home/Home';

// Demo
import Checkout from './components/Landing/Checkout';
import Custom from './components/Landing/Custom';
import Blank from './components/Landing/Blank';
import NotFound from './components/Landing/NotFound';
// import Catalog from './components/Catalog/Catalog';

// Examples
import AsyncRequest from './examples/AsyncRequest';
import Pagination from './examples/Pagination';

class App extends Component {
  componentDidMount() {
    console.log('env: ', process.env.NODE_ENV);
    console.log('git version: ', process.env.REACT_APP_VERSION);
    // console.log('gtm: ', process.env.REACT_APP_GTM);
  }

  render() {
    return (
      <Router>
        <Layout>
          <Switch>
            <LayoutDefault exact path="/" component={Home} />
            <LayoutDefault path="/catalog" component={Pagination} />
            <LayoutCheckout path="/checkout" component={Checkout} />
            <LayoutCustom path="/c/" component={Custom} />
            <LayoutBlank path="/b/" component={Blank} />
            <LayoutBlank path="/async" component={AsyncRequest} />
            <LayoutBlank path="*" component={NotFound} />
          </Switch>
        </Layout>
      </Router>
    );
  }
}

export default App;
