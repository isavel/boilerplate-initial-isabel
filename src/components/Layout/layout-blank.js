import React from 'react';
import { Route } from 'react-router-dom';

import Nav from './Nav/Nav';

const LayoutBlank = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={matchProps => (
      <div className="layout--blank">
        <Nav />
        <h1>Blank</h1>
        <Component {...matchProps} />
      </div>
    )}
  />
);

export default LayoutBlank;
