import React from 'react';
import { Route } from 'react-router-dom';

import Nav from './Nav/Nav';

const LayoutCheckout = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={matchProps => (
      <div className="layout--checkout">
        <Nav />
        <h1>Checkout</h1>
        <Component {...matchProps} />
      </div>
    )}
  />
);

export default LayoutCheckout;
