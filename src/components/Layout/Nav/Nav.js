import React from 'react';
import { Link } from 'react-router-dom';

const Nav = () => (
  <nav>
    <Link to="/">Home</Link>
    <Link to="/catalog">Catalog</Link>
    <Link to="/checkout">Checkout</Link>
    <Link to="/c/">Custom</Link>
    <Link to="/b/">Blank</Link>
    <Link to="/nyan/">404</Link>
  </nav>
);

export default Nav;
