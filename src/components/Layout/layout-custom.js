import React from 'react';
import { Route } from 'react-router-dom';

import Nav from './Nav/Nav';

const LayoutCustom = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={matchProps => (
      <div className="layout--custom">
        <Nav />
        <h1>Custom</h1>
        <Component {...matchProps} />
      </div>
    )}
  />
);

export default LayoutCustom;
