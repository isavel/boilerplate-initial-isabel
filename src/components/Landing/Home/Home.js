import React from 'react';
import './Home.scss';

import { wcaasLogo } from '../../../helpers/svg';

const Home = () => (
  <header className="App-header">
    <div className="App-logo">{wcaasLogo}</div>
    <h1>merchant</h1>
  </header>
);


export default Home;
