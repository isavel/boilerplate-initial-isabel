module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true
  },
  parser: 'babel-eslint',
  extends: 'airbnb',
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: [
    'react',
  ],
  rules: {
    'no-console': 0,
    'no-debugger': 0,
    'react/prop-types': 0,
    'react/no-unescaped-entities': 0,
    'react/jsx-filename-extension': [1, { 'extensions': ['.js', '.jsx'] }],
    'jsx-a11y/label-has-for': [2, {
      'required': { 'every': ['nesting'] },
      'allowChildren': true
    }],
    'jsx-a11y/anchor-is-valid': ['error', {
      'specialLink': [],
      'aspects': []
    }],
    'jsx-a11y/label-has-associated-control': 0,
    'linebreak-style': 0,
    'no-alert': 0,
    'react/no-unused-state': 0,
    'import/no-extraneous-dependencies': 0,
    'max-len': 0,
    'jsx-a11y/click-events-have-key-events': 0,
    'class-methods-use-this': [0, { 'exceptMethods': [] }],
    'object-curly-newline': 0,
    'jsx-a11y/no-static-element-interactions': [0, { 'handlers': [] }],
    'jsx-a11y/anchor-has-content': [0, { 'components': [] }],
    'react/no-string-refs': 0,
    'no-else-return': 0,
    'no-underscore-dangle': 0,
    'prefer-destructuring': ['error', { 'object': false, 'array': false }],
    'no-param-reassign': [2, { 'props': false }]
  },
};
